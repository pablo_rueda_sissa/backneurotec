package com.sissa.ixmakiserv.controllers.models

import com.sissa.ixmakiserv.services.ixmaki_services.IxmakiServices
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.multipart.MultipartFile
import java.util.*

@RestController
@RequestMapping("/ixmaki")
class IxmakiController {

    @Autowired
    lateinit var ixmakiServices: IxmakiServices

    @PostMapping("/enrolluser")
    fun enrollUser(@RequestParam("image") image: MultipartFile): ResponseEntity<Response> {
        try {
            val imageBytes = image.bytes
            val timeNow = Date()
            val fileName = "image-" + timeNow.time + ".png"
            ixmakiServices.enrollUser()
            val response = Response(1, "Success")
            return ResponseEntity(response, HttpStatus.OK)
        } catch (e: Exception) {
            log.error("**** Error *****" + e.message)
            e.printStackTrace()
        }

        val response = Response(1, "Success")
        return ResponseEntity(response, HttpStatus.OK)
    }

    companion object {
        private val log = LoggerFactory.getLogger(IxmakiController::class.java.name)
    }
}