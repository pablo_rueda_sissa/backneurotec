package com.sissa.ixmakiserv.controllers.models

data class Response(
    val code: Int,
    val status: String
)
