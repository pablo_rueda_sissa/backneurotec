package com.sissa.ixmakiserv

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class IxmakiservApplication

fun main(args: Array<String>) {
	runApplication<IxmakiservApplication>(*args)
}
