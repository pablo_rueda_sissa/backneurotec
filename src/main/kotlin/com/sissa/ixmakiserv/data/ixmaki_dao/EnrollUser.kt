package com.sissa.ixmakiserv.data.ixmaki_dao

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface EnrollUser : JpaRepository<Int,Long> {

    @Query("SELECT ")
    fun findById(id: Int): Int
}