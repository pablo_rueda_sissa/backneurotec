package com.sissa.ixmakiserv.services

interface GenerateTemplateUseCase {
    fun generateTemplateFromImage(): String
}