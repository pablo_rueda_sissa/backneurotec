package com.sissa.ixmakiserv.busises_logic.use_cases

import com.neurotec.biometrics.NBiometricStatus
import com.neurotec.biometrics.NFace
import com.neurotec.biometrics.NSubject
import com.neurotec.licensing.NLicense
import com.neurotec.licensing.NLicenseManager
import com.neurotec.plugins.NDataFileManager
import com.sissa.ixmakiserv.busises_logic.neurotechnology.BiometricClient
import com.sissa.ixmakiserv.busises_logic.neurotechnology.LibraryManager
import com.sissa.ixmakiserv.busises_logic.neurotechnology.Utils
import com.sissa.ixmakiserv.services.GenerateTemplateUseCase
import java.util.*


class GenerateTemplaeteUseCaseImpl: GenerateTemplateUseCase {
    override fun generateTemplateFromImage(): String {

        NDataFileManager.getInstance().addFromDirectory(System.getProperty("user.dir")+"/ndfs",true)
        LibraryManager.initLibraryPath()
        val license = "FaceExtractor";

        System.out.println("\tSetting trial mode")
        NLicenseManager.setTrialMode(true)

        try {
            println("\tObtaining licenses")
            if (!NLicense.obtain("/local", 5000, license)) {
                System.err.format("Could not obtain license: %s%n", license)
                System.exit(-1)
            }
            println("\tLicences obtained")

            val subject = NSubject()
            val nFace = NFace()

            // TODO Cambiar èsto por los bytes de la imagen

            nFace.fileName =
                "C:\\Users\\pepe_\\Downloads\\spring-boot-image-upload-main\\spring-boot-image-upload-main\\src\\main\\java\\com\\yohan\\imageupload\\mocks/pepe.jpg"
            println("\tFile name setted")
            subject.faces.add(nFace)

            println("ESTOY AQUI ANTES DE LA CAGAZON")
            val status = BiometricClient.instance.createTemplate(subject)
            if (subject.faces.size > 1) System.out.format("Found %d faces\n", subject.faces.size - 1)

            println("del otro lado")
            // List attributes for all located faces
            for (nface in subject.faces) {
                for (attribute in nface.objects) {
                    println("Face:")
                    System.out.format(
                        "\tLocation = (%d, %d), width = %d, height = %d\n",
                        attribute.boundingRect.bounds.x,
                        attribute.boundingRect.bounds.y,
                        attribute.boundingRect.width,
                        attribute.boundingRect.height
                    )
                    if (attribute.rightEyeCenter.confidence > 0 || attribute.leftEyeCenter.confidence > 0) {
                        println("\tFound eyes:")
                        if (attribute.rightEyeCenter.confidence > 0) {
                            System.out.format(
                                "\t\tRight: location = (%d, %d), confidence = %d%n",
                                attribute.rightEyeCenter.x,
                                attribute.rightEyeCenter.y,
                                attribute.rightEyeCenter.confidence
                            )
                        }
                        if (attribute.leftEyeCenter.confidence > 0) {
                            System.out.format(
                                "\t\tLeft: location = (%d, %d), confidence = %d%n",
                                attribute.leftEyeCenter.x,
                                attribute.leftEyeCenter.y,
                                attribute.leftEyeCenter.confidence
                            )
                        }
                    }
                }
            }
            if (status == NBiometricStatus.OK) {
                println("Template extracted")
                // Save compressed template to file
                // NFile.writeAllBytes(args[1], subject.getTemplate().save());
                println("Template saved successfully ")
                val stringTemplate: String = Base64.getEncoder().encodeToString(subject.templateBuffer.toByteArray())
                println("Template: $stringTemplate")
            } else {
                System.out.format("Extraction failed: %s%n", status.toString())
                System.exit(-1)
            }
            return ""
        } catch (th: Throwable) {
            Utils.handleError(th)
            return ""
        }
    }
}