package com.sissa.ixmakiserv.busises_logic.neurotechnology

import com.neurotec.biometrics.NTemplateSize
import com.neurotec.biometrics.client.NBiometricClient

object BiometricClient {
    val instance = NBiometricClient()
    init {
        instance.facesTemplateSize = NTemplateSize.LARGE
    }
}