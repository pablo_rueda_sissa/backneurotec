package com.sissa.ixmakiserv.services.ixmaki_services

import com.sissa.ixmakiserv.busises_logic.use_cases.GenerateTemplaeteUseCaseImpl
import com.sissa.ixmakiserv.data.ixmaki_dao.EnrollUser
import com.sissa.ixmakiserv.services.GenerateTemplateUseCase
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException

@Service
class IxmakiServicesImpl : IxmakiServices {

    /*@Autowired
    lateinit var enrollUser: EnrollUser*/

    lateinit var generateTemplateUseCase: GenerateTemplateUseCase

    override fun enrollUser(): String {
        try {
            generateTemplateUseCase = GenerateTemplaeteUseCaseImpl()
            val templateString = generateTemplateUseCase.generateTemplateFromImage()
            return templateString
        } catch (e: Exception){
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, e.message + "NO funciona aesta imerda", e);
        }
    }
}